//
//  Vibration.swift
//  UpUVibe
//
//  Created by Catherine Chan on 1/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import Foundation
import RealmSwift

class Vibration : Object {
    @objc dynamic var dateCreated : Date?
    @objc dynamic var score : Double = 0.0
}
