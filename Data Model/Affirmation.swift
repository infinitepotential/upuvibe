//
//  Affirmation.swift
//  UpUVibe
//
//  Created by Catherine Chan on 8/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import Foundation
import RealmSwift

class Affirmation : Object {
    @objc dynamic var content : String = ""
}

class AffirmationWrapper: Object {
    var list = List<Affirmation>()
}


