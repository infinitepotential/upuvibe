//
//  Data.swift
//  UpUVibe
//
//  Created by Catherine Chan on 7/12/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import Foundation
import RealmSwift

class Journal : Object {
    @objc dynamic var dateCreated : Date?
    @objc dynamic var title :  String = ""
    @objc dynamic var content : String = ""

}
