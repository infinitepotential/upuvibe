//
//  MyTips.swift
//  UpUVibe
//
//  Created by Catherine Chan on 24/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import Foundation
import RealmSwift

class Tip : Object {
    @objc dynamic var content : String = ""
}

class TipsWrapper: Object {
    var list = List<Tip>()
}

