//
//  VibrationViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 31/12/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import RealmSwift
import Crashlytics

protocol LoadData {
    func loadData()
}


class VibrationViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
   

    @IBOutlet weak var dateInputTextField: UITextField!
    @IBOutlet weak var uiPickerView: UIPickerView!
    @IBOutlet weak var noteTitleLabel: UILabel!
    @IBOutlet weak var addNoteButton: UIButton!
    
    var delegate : LoadData?
    
    let realm = try!Realm()
    var date = Date() // store the current date in here
    let dateFormatter = DateFormatter()
    
    let datePicker = UIDatePicker()
    let newJournal = Journal() // we declare in function is because everytime it is a new journal object.
    let vibration = Vibration()
   
    
    let vibrationScale = ["Enlightment 700+", "Peace 600", "Joy 540", "Love 500", "Reason 400", "Acceptance 350", "Willingness 310", "Neutrality 250", "Courage 200", "Pride 175", "Anger 150", "Desire 125", "Fear 100", "Grief 75", "Apathy 50", "Guilt 30", "Shame 20"]
    let vibrationScore = [700.0, 600.0, 540.0, 500.0, 400.0, 350.0, 310.0, 250.0, 200.0,  175.0, 150.0, 125.0, 100.0, 75.0, 50.0, 30.0, 20.0]
    var vibrationColor : [UIColor] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Track how many people add new score
        Answers.logContentView(withName: "Add Vibration", contentType: "Add Vibration", contentId: "1063")
        
         addNoteButton.applyButtonDesign(hex: 0x420093)
       // navigationItem.backBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: nil)
        //self.navigationController?.navigationBar.topItem?.title = "Cancel"
        // self.navigationItem.setHidesBackButton(true, animated:true)
        vibration.score = 700.0
        vibration.dateCreated = date
        uiPickerView.delegate = self
        uiPickerView.dataSource = self
        vibrationColor = [UIColor(rgb: 0xAB00CE), UIColor(rgb: 0x8E0CDD), UIColor(rgb: 0x6B16EF), UIColor(rgb: 0x4600FF), UIColor(rgb: 0x2C42FF), UIColor(rgb: 0x01AEEF), UIColor(rgb: 0x00C08F), UIColor(rgb: 0x04D83B), UIColor(rgb: 0x1BD92E), UIColor(rgb: 0x99D91B),UIColor(rgb: 0xE4D700), UIColor(rgb: 0xFFBF00), UIColor(rgb: 0xFF9A0E), UIColor(rgb: 0xFF6A00), UIColor(rgb: 0xFF2F00),UIColor(rgb: 0xFF0505), UIColor(rgb: 0xFA0202)   ]
        showTime()
        createDatePicker()
       
        
    }
   
    
    //MARK: Select time from DatePicker or use current time

    func showTime() {
        dateFormatter.dateFormat = "E, d MMM yyyy h:mm a"
        dateInputTextField.text = "\(dateFormatter.string(from: date))"
       
    }
    
    //MARK: CREATE DATE PICKER
    func createDatePicker () {
        datePicker.datePickerMode = .dateAndTime
        
        // assign Date Pikcer to our text field
        dateInputTextField.inputView = datePicker
        
        //create tool bar to display button along the bottom edge of your interface
        let toolBar = UIToolbar()
        toolBar.sizeToFit() // without this I could not see the tool bar button. resize and adjust the subview
        
        //add a done button on this tool bar
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClick))
       
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems( [flexibleSpace, doneButton], animated: true)

        
        dateInputTextField.inputAccessoryView = toolBar
       
    }
   
    // when click the done button, the new date will be presented in the datePickerTextField
    
    @objc func doneClick() {
        //format the display in the textfield
       dateFormatter.dateFormat = "E, d MMM yyyy h:mm a"
      //  dateFormatter.dateStyle = .short
        dateInputTextField.text =  dateFormatter.string(from: datePicker.date)
        date = datePicker.date
        self.view.endEditing(true)
    }
    
    
    //MARK: Select Vibration Score from Picker View
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return vibrationScale.count
    }
    

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "San Francisco", size: 20)
            pickerLabel?.textAlignment = .center
        }
        pickerLabel?.text = vibrationScale[row]
        pickerLabel?.textColor = vibrationColor[row]
        
        return pickerLabel!
    }
    
    //4th method, tell the picker what to do when the user selects a particular row.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            vibration.score = vibrationScore[row]
 
    }
    
    
   
    
    //MARK: Adding Note (journal) for current vibration
    @IBAction func addNoteButtonPressed(_ sender: UIButton) {
        
       
            
        // MARK: TRACK HOW MANY USERS USE ADD NOTE BUTTON IN NEW VIBRATION PAGE
            Answers.logCustomEvent(withName: "New Journal", customAttributes: ["Method":"Journal from Vibration"])
    
        
        var textField = UITextField()
    
        
        let alert = UIAlertController (title: "Add a quick note in your journal", message: "You can always add more details later.", preferredStyle: .alert)
        
        
        let action = UIAlertAction(title: "Add Note", style: .default ) { (Action) in
    
            self.newJournal.title = textField.text!
            self.noteTitleLabel.text = textField.text!
        }
        
        alert.addTextField{ (alertTextField) in
            
            alertTextField.placeholder = "Note"
            
            textField = alertTextField
            
        }

     
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
        
    
    
    }
    
    func saveJournal(_ journal: Journal) {
        do {
            try realm.write {
                realm.add(journal)
            }
        } catch {
            print("Error initalising and saving realm:\(error)")
        }
        
        //    self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Saving the Vibration Score and Note
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        if newJournal.title != "" {
            newJournal.dateCreated = date
            saveJournal(newJournal)
        }
        saveVibration(vibration)
        
    }
    
   
    
 
    func saveVibration(_ vibration: Vibration) {
        
        vibration.dateCreated = date
        
        do {
            try realm.write {
                realm.add(vibration)
            }
        } catch {
            print("Error initalising and saving realm:\(error)")
        }
        delegate?.loadData()
        
        self.navigationController?.popViewController(animated: true)
    }
   
}


