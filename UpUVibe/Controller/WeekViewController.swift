//
//  WeekViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 28/12/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import Charts
import RealmSwift
import Crashlytics

class WeekViewController: UIViewController {
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lastWeekButton: UIButton!
    @IBOutlet weak var thisWeekButton: UIButton!
    
    
    
    var dataEntry : [BarChartDataEntry] = []
    let timeScale = ["Mon","Tue","Wed","Thur","Fri","Sat","Sun"]
    var vibrationScore :[Double] = []
    
    let timeInterval = 24.0 * 3600.0 //seconds for a day
    var calendar = Calendar(identifier: .gregorian)
    
    var dayArray : [Date] = []
    var date = Date()
    let realm = try! Realm()
    var vibration : [Vibration] = []
  //  var vibrationArray : [[Vibration]] = []
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Track how many people use weekly score
        Answers.logContentView(withName: "Weekly Vibration", contentType: "Weekly Vibration", contentId: "1061")
        
        lastWeekButton.applyButtonDesign(hex: 0x420093)
        thisWeekButton.applyButtonDesign(hex: 0x420093)
        
//        print(monthInterval.start)
//        print(monthInterval.end)
         loadData()
    }
    
    //MARK: Load data from Realm
    func loadData(){
    //  let monthInterval = calendar.dateInterval(of: .month, for: date)!
        var count = 1
        dataEntry = []
        vibrationScore = []
        dayArray = []
        vibration = []
        
        let lastSun = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: date))!
        dayArray.append(lastSun.addingTimeInterval(timeInterval)) //Append Monday as first day in dayArray
      // print(dayArray)
        for i in 0...6 {
            var score : Double = 0
            let nextDay = dayArray[i].addingTimeInterval(timeInterval)
            dayArray.append(nextDay)
            
           vibration = Array(realm.objects(Vibration.self).filter("dateCreated BETWEEN %@", [dayArray[i], dayArray[i+1]]))
           
        
                for j in 0..<vibration.count {
                    score = score + vibration[j].score
                }
            if vibration.count != 0 {
                count = vibration.count
            }
            vibrationScore.append(score/Double(count)) // append daily average score to vibrationScore array
     
    }
        barChartSetUp()
    }
    
    
    //MARK: SET UP BAR CHART GRAPH
    func barChartSetUp(){
        //What happen when no data set up, empty string for dataPoints and Values
        barChartView.noDataTextColor = UIColor.gray
        barChartView.noDataText = "Please enter Data for the chart."
        barChartView.backgroundColor = UIColor.white
        
        
        //Bar Chart Animation
        barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInSine)
        
        for i in 0..<timeScale.count {
            let dataPoint = BarChartDataEntry(x: Double(i), y: Double(vibrationScore[i]))
            dataEntry.append(dataPoint)
        }
        
        let chartDataSet = BarChartDataSet(entries: dataEntry, label: "Average Daily Vibration Score")
        let chartData = BarChartData()
        chartData.addDataSet(chartDataSet)
        chartData.setDrawValues(false)
        chartDataSet.colors = [UIColor.purple]
        
        
        //Axes Set up
        
        let formatter : ChartFormatter = ChartFormatter(labels: timeScale)
        barChartView.data = chartData
        barChartView.xAxis.labelPosition = .bottom
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.xAxis.valueFormatter = formatter
  
        
        barChartView.chartDescription?.enabled = false
        barChartView.legend.enabled = false
       
        barChartView.rightAxis.enabled = false
        barChartView.leftAxis.drawGridLinesEnabled = false
        barChartView.leftAxis.axisMaximum = 750
        barChartView.leftAxis.axisMinimum = 0
        barChartView.leftAxis.minWidth = 30
       
        
        
    }
    
    @IBAction func lastWeekButtonPressed(_ sender: UIButton) {
        titleLabel.text =  "Last Week Score"
        date = Calendar.current.date(byAdding: .weekOfYear, value: -1, to: date)!
        loadData()
    }
    
    @IBAction func thisWeekButtonPressed(_ sender: UIButton) {
         titleLabel.text =  "This Week Score"
        date = Date()
        loadData()
    }
    
    
}

