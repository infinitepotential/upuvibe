//
//  ViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 21/11/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import Crashlytics

class ViewController: UIViewController {
   
    @IBOutlet weak var SignUp_Button: UIButton!
    @IBOutlet weak var LogIn_Button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
    // TODO: Track the user launching of the app
        Answers.logContentView(withName: "Launch Screen", contentType: "Launch", contentId: "1010")


       SignUp_Button.applyButtonDesign(hex: 0x420093)// 0x420093
        LogIn_Button.applyButtonDesign(hex: 0x730093)
        
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
   // self.navigationItem.hidesBackButton = true
 // self.navigationItem.backBarButtonItem?.tintColor = UIColor.clear
    }


}
