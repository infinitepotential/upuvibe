//
//  SignUpViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 22/11/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import Crashlytics

class SignUpViewController: UIViewController { //contentId: "1011"
    
    @IBOutlet weak var eMailTextField: BorderUITExtField!
    @IBOutlet weak var passWordTextField: BorderUITExtField!
    @IBOutlet weak var confirmPWTextField: BorderUITExtField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var termButtonOutlet: UIButton!
    
    let yourAttributes : [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10),
        NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
    //.styleDouble.rawValue, .styleThick.rawValue, .styleNone.rawValue
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Track how many people go to sign up page
        Answers.logContentView(withName: "SignUp Page", contentType: "SignUp", contentId: "1011")

         signUpButton.applyButtonDesign(hex: 0x420093)
        
        let attributeString = NSMutableAttributedString(string: "Terms & Condition",
                                                        attributes: yourAttributes)
        termButtonOutlet.setAttributedTitle(attributeString, for: .normal)
    }
    
    
       //MARK: Show Error Alert when sign up fail
    func errorAlert (_ title : String) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            print("Success")
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    
    
    @IBAction func signUpButton(_ sender: UIButton) {
        
        
       
        //TODO: Set up a new user on our Firebase Database
        if passWordTextField.text! != confirmPWTextField.text! || passWordTextField.text!.count < 7 {
          errorAlert("Password invalid or doesn't match, please enter again.")
        } else {
            //MARK: Show Log-In Progress
            SVProgressHUD.show()
            Auth.auth().createUser(withEmail: eMailTextField.text!, password: passWordTextField.text!) { (user, error) in
                if error != nil {
                    print(error!)
                    SVProgressHUD.dismiss()
                    self.errorAlert("Invalid email or No connection, please try again.")
                    
                    // TODO: Track the user sign up failure rate
                    Answers.logSignUp(withMethod: "E-mail",
                                                success: false,
                                                customAttributes: nil)
                } else {
                    //success
                    // TODO: Track the user sign up success rate
                    Answers.logSignUp(withMethod: "E-mail",
                                                success: true,
                                                customAttributes: nil)
                    SVProgressHUD.dismiss()
                    self.performSegue(withIdentifier: "goToHome", sender: self)
                }
            }

        }
        
        
        
        }
        
        

    @IBAction func termButtonPressed(_ sender: UIButton) {
        //MARK: Track how many people go to see the terms
        Answers.logContentView(withName: "Terms", contentType: "Terms From SignUp", contentId: "1071")
        
        if let url = URL(string: "https://upuvibe.com/terms-conditions/") {
            UIApplication.shared.open(url, options: [:])
    }
    
    }
    
}



