//
//  TableViewCell.swift
//  UpUVibe
//
//  Created by Catherine Chan on 14/12/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var JournalDayLabel: UILabel!
    @IBOutlet weak var JournalTitleLabel: UILabel!
    @IBOutlet weak var JournalTimeLabel: UILabel!
    var csvText = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
