//
//  LoginViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 22/11/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class LoginViewController: UIViewController {
    @IBOutlet weak var emailTextField: BorderUITExtField!
    
    @IBOutlet weak var passWordTextField: BorderUITExtField!
    
    @IBOutlet weak var logInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logInButton.applyButtonDesign(hex: 0x730093)
        emailTextField.tintColor = UIColor(rgb: 0x730093)
        passWordTextField.tintColor =  UIColor(rgb: 0x730093)
        
    }
    
    //MARK: Set up Error Alert when log-in fail
    func errorAlert (_ title : String) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            print("Success")
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func logInButton(_ sender: UIButton) {
        SVProgressHUD.show()
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passWordTextField.text!) { (results, error) in
            if error != nil {
                SVProgressHUD.dismiss()
                  self.errorAlert("Incorrect password or no connection, please try again.")
            } else {
                print("Log In successful")
                SVProgressHUD.dismiss()
                self.performSegue(withIdentifier: "goToHome", sender: self)
                
                }
            }
       
        }
    
    
 

    @IBAction func resetPasswordPressed(_ sender: Any) {
        let resetPasswordAlert = UIAlertController(title: "Reset password?", message: "Enter email address", preferredStyle: .alert)
        resetPasswordAlert.addTextField { (textField) in
            textField.placeholder = "Enter email address"
        }
        resetPasswordAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        resetPasswordAlert.addAction(UIAlertAction(title: "Reset", style: .default, handler: { (action) in
            let resetEmail = resetPasswordAlert.textFields?.first?.text
            Auth.auth().sendPasswordReset(withEmail: resetEmail!, completion: { (error) in
                
                if let error = error {
                    let resetFailedAlert = UIAlertController(title: "Reset Failed", message: error.localizedDescription, preferredStyle: .alert)
                    resetFailedAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(resetFailedAlert, animated: true, completion: nil)
                } else {
                    let resetEmailSentAlert = UIAlertController(title: "Reset email sent successfully", message: "Please check your email", preferredStyle: .alert)
                    resetEmailSentAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(resetEmailSentAlert, animated: true, completion: nil)
                }
                
                
            })
        }))
        //PRESENT ALERT
        self.present(resetPasswordAlert, animated: true, completion: nil)
    }
    
    
}
