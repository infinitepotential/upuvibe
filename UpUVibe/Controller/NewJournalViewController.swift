//
//  NewJournalViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 7/12/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import RealmSwift
import Crashlytics

protocol LoadJournals {
    func loadJournal()
}

class NewJournalViewController: UIViewController, UITextViewDelegate {
      
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var titleTextField: BorderUITExtField!
    @IBOutlet weak var journalContentsTextView: UITextView!
    
    var delegate : LoadJournals?
    let realm = try! Realm()
   
    var date = Date()
    let dateFormatter = DateFormatter()
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Track how many people create new journal
        Answers.logContentView(withName: "New Journal", contentType: "Journal From Summary", contentId: "1041")
        
        journalContentsTextView.text = "Please write here..."
        journalContentsTextView.textColor = UIColor.lightGray
        journalContentsTextView.delegate = self
        journalContentsTextView.tintColorDidChange()
        titleTextField.tintColorDidChange()
        showTime()
        createDatePicker()
        
    }
    

    //MARK: CREATE DATE PICKER VIEW
    //??? Repetative Codes with others controllers, vibration journlas, think of classes whatever to reduce it.
    func showTime() {
        dateFormatter.dateFormat = "E, d MMM yyyy h:mm a"
        dateTextField.text = dateFormatter.string(from: date)
        
    }
    
    func createDatePicker () {
        // format the display of our datepicker View
        datePicker.datePickerMode = .dateAndTime
        
        // assign Date Pikcer to our text field
        dateTextField.inputView = datePicker
        
        //create tool bar to display button along the bottom edge of your interface
        let toolBar = UIToolbar()
        toolBar.sizeToFit() // without this I could not see the tool bar button. resize and adjust the subview
        
        //add a done button on this tool bar
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClick))
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems( [flexibleSpace, doneButton], animated: true)
        
        
        dateTextField.inputAccessoryView = toolBar
        
    }
    
    // when click the done button, the data will be presented in the datePickerTextField
    
    @objc func doneClick() {
        //format the display in the textfield
        dateFormatter.dateFormat = "E, d MMM yyyy h:mm a"
        //  dateFormatter.dateStyle = .short
        dateTextField.text =  dateFormatter.string(from: datePicker.date)
        date =  datePicker.date
        self.view.endEditing(true)
    }
    
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        let newJournal = Journal() // we declare in function is because everytime it is a new journal object.
        if journalContentsTextView.text! == "Please write here..."  { 
            newJournal.content = ""
           
        } else {
            newJournal.content = journalContentsTextView.text!
          
        }
        
        newJournal.dateCreated = date
        newJournal.title = titleTextField.text!
      
        save(newJournal)
    }
    
    //MARK: UPDATE REALM DATA
func save(_ journal: Journal) {
        do {
            try realm.write {
                realm.add(journal)
            }
        } catch {
            print("Error initalising and saving realm:\(error)")
        }
        delegate?.loadJournal()
        self.navigationController?.popViewController(animated: true)
    }

    
    //MARK: TEXTVIEW DID BEGIN EDITING
    func textViewDidBeginEditing(_ textView: UITextView) {
            textView.text = ""
            textView.textColor = UIColor.black
        
    }

}

