//
//  UIButton.swift
//  UpUVibe
//
//  Created by Catherine Chan on 4/02/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import UIKit

extension UIButton {
    func applyButtonDesign (hex: Int ){
        self.backgroundColor = UIColor(rgb: hex )
        self.layer.cornerRadius = self.frame.height/2  //This will round the corner up completely. becasue it is going to be both corners, so need to divide it by 2.  You can also use a number 7, this is not rounding the corner completely
        self.setTitleColor(UIColor.white, for: .normal)
        
    }
    
}
