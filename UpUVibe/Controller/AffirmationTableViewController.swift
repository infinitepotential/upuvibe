//
//  AffirmationTableViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 8/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import RealmSwift
import Crashlytics

class AffirmationTableViewController: SwipeTableTableViewController { // UITextFieldDelegate is important to make the things work , UITextFieldDelegate

let realm = try! Realm()


    var affirmations  : List<Affirmation>? // List of Affirmation Objects
    var affirmationWrapper = AffirmationWrapper() // create an empty list object (add a new instance into dataBase if there is none. )
    var affirmation = Affirmation()
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
          //MARK: Track how many people use affirmation page
        Answers.logContentView(withName: "Affirmation", contentType: "Affirmation", contentId: "1032")
        
        // MARK: Register "newTableViewCell"
        // with nibFile: "NewTableViewCell", which is the same name of xib file, just look the xib name.
        tableView.register(UINib(nibName: "AffirmationCell", bundle: nil),  forCellReuseIdentifier: "affirmationCell")
      
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.tableFooterView = UIView() // remove extra cell
        
        self.tableView.isEditing = false
        loadAffirmation()
      //  tableView.setEditing(false, animated: true)
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return affirmations?.count ?? 1
    
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "affirmationCell", for: indexPath) as! AffirmationCell // the 1st is the Identifier : indentity of the cell, the 2nd is the class of the cell

        cell.delegate = self
        
       if let currentAffirmation = affirmations?[indexPath.row]
        { // let currentAffirmation =  dataWrapper.list[indexPath.row]
            cell.label.textColor = UIColor.black
            cell.label.text =  currentAffirmation.content
            cell.label.lineBreakMode = .byWordWrapping
            cell.label.numberOfLines = 0

            
        } else {
            cell.label.text = "Please add your first Affirmation."
            cell.label.textColor = UIColor.lightGray
        }
        

        return cell
    }
  
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    
    //MARK: Reorder Affirmation Method
    
    //Show the reoder symbol
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    //Determine whehter the tableView shall indent while editing
    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
    // Determine whether a particular can be move
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        try! realm.write {
            
            let movedObject = affirmations![sourceIndexPath.row]
            affirmations!.remove(at: sourceIndexPath.row)
            affirmations!.insert(movedObject, at: destinationIndexPath.row)
          
        }


        
         tableView.reloadData()
    }
    
    
    //MARK: Update Realm database when delete
    override func updateModel (at indexPath: IndexPath) {
        
        
        if let affirmationforDeletetion = self.affirmations?[indexPath.row] {
            //When the affirmation instance is deleted, the information in dataWrapper will also be deleted.
            do {
                try self.realm.write {
                self.realm.delete(affirmationforDeletetion)
    
                }
             
                self.loadAffirmation()
            } catch {
                print("Error deleting Affirmation:\(error)")
            }
          //  tableView.reloadData()
          // self.loadAffirmation()

        }

    }

    //MARK: Load affirmation results
    func loadAffirmation() {
       //take the first instance in the DataWrapper in the query and get its list property (which is a list of affrimation objects - List<Affirmation>() return to the results.
        if let results = (realm.objects(AffirmationWrapper.self).first?.list) { // Results to a query are not copies of your data: modifying the results of a query (within a write transaction) will modify the data on disk directly.
            affirmations = results // affirmations is also a list of affirmation Objects, it is of the same dataType
            affirmationWrapper.list = results  // affirmations is definetely not nil, safe to unwrap to the dataWrapper() object and save into its list property.  Basically it is to take the data out of itself and save it to an empty list object.
        }
        
        tableView.reloadData()
        
    }
 
    @IBAction func reOrderButtonPressed(_ sender: UIButton) {//if the tableView is in editing mode, it will set a editing mode for us to reorder
        if (tableView.isEditing == true) {
            tableView.setEditing(false, animated: true)
        } else {
            tableView.setEditing(true, animated: true)
        }
        
        
    }
    
    
    //MARK: Adding New Affirmation
    
    @IBAction func newAffirmationButtonPressed(_ sender: UIButton) {
            var textField = UITextField()
        
        
            let alert = UIAlertController (title: "Add New Affirmation", message: "", preferredStyle: .alert)
        
        
            let action = UIAlertAction(title: "Add", style: .default ) { (Action) in
                
            let newAffirmation = Affirmation()

                newAffirmation.content = textField.text!
        
            self.saveAffirmation(newAffirmation)
            
        }
        
            alert.addTextField{ (alertTextField) in
        
            alertTextField.placeholder = "I am awesome!"
        
            textField = alertTextField
        
            }
        
        
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(action)
        
            present(alert, animated: true, completion: nil)
    }
    

    
    func saveAffirmation(_ affirmation: Affirmation) {
       
        do {
            try realm.write { // append the new affirmation to the affirmationWrapper.list of object
                affirmationWrapper.list.append(affirmation)
                
                if affirmations == nil { //if there is no affirmationWrapper instance in the database, add a new one to the database. otherwise Realm seems to know that the affirmationWrapper is the only instance and updated it accordingly
                realm.add(affirmationWrapper)
            }
            }
        } catch {
            print("Error initalising and saving affirmation:\(error)")
        }
        
        self.loadAffirmation()
    }
    
}


