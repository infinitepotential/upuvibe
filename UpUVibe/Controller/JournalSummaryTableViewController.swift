//
//  JournalSummaryTableViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 6/12/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import RealmSwift
import Crashlytics
// import SwipeCellKit

class JournalSummaryTableViewController: SwipeTableTableViewController, LoadJournals, UpdateJournal {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: Format Date and Time
    let formatterD = DateFormatter()
  // let formatterY = DateFormatter()
    let formatterT = DateFormatter()
    
    // MARK: Realm Declaration
    let realm = try! Realm()
    var journals : Array<Journal> = []
    
   // var journals : Results<Journal>?
    var journal = Journal()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Track how many people use journals page
        Answers.logContentView(withName: "Journal Summary", contentType: "Journal Summary", contentId: "1040")
        
        tableView.tableFooterView = UIView()
        loadJournal()

        // MARK: Register "newTableViewCell" with nibFile: "NewTableViewCell"
        tableView.register(UINib(nibName: "NewTableViewCell", bundle: nil),  forCellReuseIdentifier: "newTableViewCell")
        
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return journals.count>0 ? journals.count : 1
      
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //declare the cell with identifier  "newTableViewCell", this will override the cell identifier in Swipeable cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "newTableViewCell", for: indexPath) as! NewTableViewCell
        cell.delegate = self
        
//        // Extract Realm Data
        if journals.count != 0 {
         let currentJournal = journals[indexPath.row]  // if the info is not nil, then show the journal date
 //        formatter.dateFormat = "E, d MMM yyyy h:mm a"
            formatterD.dateFormat = " d MMM yyyy"
           //  formatterY.dateFormat = "yyyy"
             formatterT.dateFormat = "E,h:mm a"


            cell.journalDayUILabel.text = formatterD.string(from: currentJournal.dateCreated!)
//            cell.journalYearUILabel.text = formatterY.string(from: currentJournal.dateCreated!)
            cell.journalTimeUILabel.text = formatterT.string(from: currentJournal.dateCreated!)
            cell.journalTitleUILabel.text = currentJournal.title
            cell.textLabel?.text  = ""

        } else {// otherwise ask the user to enter new journal
            cell.journalDayUILabel.text = ""
            cell.journalTimeUILabel.text = ""
            cell.journalTitleUILabel.text = ""
            cell.textLabel?.text = "Please add your first note ..."
             cell.textLabel?.textColor = .lightGray
        }

        
        return cell
    }
    

    //MARK: TableView Delegate Method
    //When the row is selected,  show the selected journal in a single journal screen. When add new journal, go to the new journal controller
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if journals.count > 0 {
            performSegue(withIdentifier: "goToSingleJournal", sender: self)
            
        }
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToNewJournal" {
            let destinationVC1 = segue.destination as! NewJournalViewController
            destinationVC1.delegate = self
        }
        
        // if the segue to edit is triggered, set the property of SingleJournalViewController to the value of the selected journals. 
        if segue.identifier == "goToSingleJournal" && journals.count > 0  {
            if let indexPath = tableView.indexPathForSelectedRow {
            let   destinationVC2 = segue.destination as! SingleJournalViewController
        destinationVC2.selectedJournal = journals[indexPath.row]
                journal = journals[indexPath.row]
                destinationVC2.delegate = self
            }
        
        }
    }
    
    
    func loadJournal() {
        
        journals = Array(realm.objects(Journal.self).sorted(byKeyPath: "dateCreated", ascending: false))
        tableView.reloadData()
        
    }
    
    
    //MARK: UPDATE REALM DATA
    override func updateModel (at indexPath: IndexPath) {
        if journals.count>0 {
            let journalforDeletetion = self.journals[indexPath.row]
            do {
                try self.realm.write {
                    self.realm.delete(journalforDeletetion)
                }
                self.loadJournal()
            } catch {
                print("Problem deleting cell")
            }
        }
        
    }
    
    func updateJournal(_ editedJournal : Journal) {
        
        try! realm.write {
            journal.title = editedJournal.title
            journal.content = editedJournal.content
        }
        
        loadJournal()
    }
    
   
    
}





