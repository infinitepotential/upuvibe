//
//  TipsTableViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 30/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import Crashlytics

class TipsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Track how many people go to see the tips page
        Answers.logContentView(withName: "Tips Page", contentType: "Tips", contentId: "1030")
        
    }

   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToPowerPostures" {
            //MARK: Track how many people go to seek help to use the app
            Answers.logContentView(withName: "Power Postures", contentType: "Power Postures", contentId: "1031")
        }
    }
    

  
}
