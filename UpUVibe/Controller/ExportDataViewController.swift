//
//  ExportDataViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 13/03/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI


class ExportDataViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var dateBeginTextField: UITextField!
    @IBOutlet weak var dateEndTextField: UITextField!
    
    var date = Date()
    let dateFormatter = DateFormatter()
    let dateFormatter2 = DateFormatter()
    let beginDatePicker = UIDatePicker()
    let endDatePicker = UIDatePicker()
    let loc = Locale(identifier: "en_NZ")
    let components = DateComponents(day: 1, second: -1)
    var beginDate = Calendar.current.startOfDay(for: Date())
    var endDate = Date()
    let realm = try! Realm()
    var vibrations : [Vibration] = []
    var journals : [Journal] = []
    var array : [String] = []
    var csvText = ""
    override func viewDidLoad() {
        super.viewDidLoad()
       
        endDate = Calendar.current.date(byAdding: components, to: Calendar.current.startOfDay(for: Date()))!
        showTime()
        // change the date display format from m/d/year -> d/m/year
        beginDatePicker.locale = loc
        beginDatePicker.datePickerMode = .date
        
        
        beginDatePicker.addTarget(self, action: #selector(updateDateField), for:UIControl.Event.valueChanged)
        dateBeginTextField.inputView = beginDatePicker

        endDatePicker.locale = loc
        endDatePicker.datePickerMode = .date
        endDatePicker.addTarget(self, action: #selector(updateDateField), for:UIControl.Event.valueChanged)
        dateEndTextField.inputView = endDatePicker

    }
    
    //show today's time in the selector
    func showTime() {
        dateFormatter.dateFormat = "d MMM yyyy"
        dateBeginTextField.text = "\(dateFormatter.string(from: date))"
        dateEndTextField.text = "\(dateFormatter.string(from: date))"
    }
    
   
    @objc func updateDateField(sender: UIDatePicker) {
        if sender == beginDatePicker
        {
            dateBeginTextField.text = dateFormatter.string(from: sender.date)
            beginDate = Calendar.current.startOfDay(for: sender.date)
        }
        if sender == endDatePicker
        {
            dateEndTextField.text = dateFormatter.string(from: sender.date)
        
            endDate = Calendar.current.date(byAdding: components, to: Calendar.current.startOfDay(for: sender.date))!
        }
        
        
    }
    
    @IBAction func exportButtonPressed(_ sender: UIButton) {
        
        if beginDate > endDate {
            let dateErrorAlert = UIAlertController(title: "Begin Date is later than End Date", message: "Please re-enter your date again", preferredStyle: .alert)
            dateErrorAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(dateErrorAlert, animated: true, completion: nil)
        
        } else {
            dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm"
            print("beginDate:\(dateFormatter2.string(from: beginDate))")
            print("endDate:\(dateFormatter2.string(from: endDate))")
            export()
        }
        
    }
    
    func export() {
        
        let fileName = "Vibration_data.csv" // create a filename and a path to the file in the temporary directory.
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
      
        //Then you want to create the initial text for the csv file
         csvText = "Vibration Data between \(dateFormatter2.string(from: beginDate)) and \(dateFormatter2.string(from: endDate))\n"
        
        csvText.append("Date,Score,Title,Note\n")
        
        // extract data from realm, store into array and populate into the cvs file
      loadData()
      
        if vibrations.count > 0 || journals.count > 0 {
        print(csvText)
   
    
        // final step of creating the csv file, we need to actually write the file to the path we created earlier.
        //You can email it, save it, open it in another app, etc.
       
                    do {
                        try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
//
//                        if MFMailComposeViewController.canSendMail() {
//                            let emailController = MFMailComposeViewController()
//                            emailController.mailComposeDelegate = self
//                            emailController.setToRecipients([])
//                            emailController.setSubject("Vibration Data from \(dateFormatter2.string(from: beginDate)) to \(dateFormatter2.string(from: endDate))")
//                            emailController.setMessageBody("Hi,\n\nThe .csv data export is attached\n\n\nSent from the UpUVibe app: http://www.upuvibe.com", isHTML: false)
//
//                            emailController.addAttachmentData(NSData(contentsOf: path!)! as Data, mimeType: "text/csv", fileName: "Vibration_data.csv")
//
//                            present(emailController, animated: true, completion: nil)
//                        }
                        
                        //EXPORTING YOUR CSV FILE using the ActivityViewController
                        let vc = UIActivityViewController(activityItems: [path!], applicationActivities: [])

                        //You can exclude certain options from showing up as a share option like so:
                        vc.excludedActivityTypes = [
                            UIActivity.ActivityType.assignToContact,
                            UIActivity.ActivityType.saveToCameraRoll,
                            UIActivity.ActivityType.postToFlickr,
                            UIActivity.ActivityType.postToVimeo,
                            UIActivity.ActivityType.postToTencentWeibo,
                            UIActivity.ActivityType.postToTwitter,
                            UIActivity.ActivityType.postToFacebook,
                            UIActivity.ActivityType.openInIBooks
                        ]
                        present(vc, animated: true, completion: nil)

                    } catch {

                        print("Failed to create file")
                        print("\(error)")
                    }

             } else {
                let exportErrorAlert = UIAlertController(title: "Error", message: "There is no data to export", preferredStyle: .alert)
                exportErrorAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(exportErrorAlert, animated: true, completion: nil)
                
                }
    
    }

//    private func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
//        controller.dismiss(animated: true, completion: nil)
//    }
    
    func loadData() {
        
        journals = Array(realm.objects(Journal.self).filter("dateCreated BETWEEN %@", [beginDate, endDate]).sorted(byKeyPath: "dateCreated", ascending: true))

        vibrations = Array(realm.objects(Vibration.self).filter("dateCreated BETWEEN %@", [beginDate, endDate]).sorted(byKeyPath: "dateCreated", ascending: true))
        
        if vibrations.count > 0 {
            for item in vibrations {
                let vibrationDate = dateFormatter2.string(from: item.dateCreated!)
                let newLine = "\(vibrationDate),\(item.score)\n"
                csvText.append(newLine)
            }
        }
        
        if journals.count > 0 {
            for item in journals {
                let journalDate = dateFormatter2.string(from: item.dateCreated!)
                let newLine = "\n\(journalDate),\(""),\(item.title),"
                let note = "\"\(item.content)\""
                csvText.append(newLine)
                csvText.append("\(note)")
            }
        }
      
    }
    
   
        
    }
    

