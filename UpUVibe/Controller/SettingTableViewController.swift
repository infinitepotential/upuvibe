//
//  SettingTableViewController.swift
//  
//
//  Created by Catherine Chan on 29/11/18.
//

import UIKit
import UserNotifications
import Firebase
import MessageUI // not sure this is sending mail??
import SVProgressHUD
import Crashlytics


class SettingTableViewController: UITableViewController, CanReceiveReminder, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var reminderSwitch: UISwitch!
    @IBOutlet weak var changeTimeTextField: UILabel!
    
    let defaults = UserDefaults.standard
    var defaultDateTime = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Track how many people use setting
        Answers.logContentView(withName: "Settings", contentType: "Settings", contentId: "1050")
      
        if let time = defaults.object(forKey: "ReminderTime") as? Date {
            defaultDateTime = time
        } // get the time out from the default
        reminderSwitch.isOn = defaults.bool(forKey: "SwitchIsOn")
        tableView.separatorStyle = .singleLine
        showReminderTime(defaultDateTime)

    }
    
    //MARK: SET UP REMINDER
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "setNewTime" {
            let changeTimeVC = segue.destination as! timePickerViewController
//          changeTimeVC.reminderTime = defaultDateTime
            changeTimeVC.delegate = self
        }
    }
    
    func showReminderTime(_ date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        changeTimeTextField.text = dateFormatter.string(from: date)
        
    }
    
    func receiveReminder(data: Date) {
        defaultDateTime = data
        defaults.set(data, forKey: "ReminderTime")
        showReminderTime(defaultDateTime)
        scheduleNotification()
        self.defaults.set(true, forKey: "SwitchIsOn")
                        DispatchQueue.main.async {
                            self.reminderSwitch.setOn(true, animated: true)
        }
    }
    
    //MARK: TOGGLE ON LOCAL NOTIFICATION
    func registerLocalNotification() {
        //        UNUserNotificationCenter.current().delegate = self // set itself as the delegate of the notification
        // MARK: request User permission of notification
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted {
                self.scheduleNotification()
            } else {
//                DispatchQueue.main.async {
//                    self.reminderSwitch.setOn(false, animated: true)
//                }
                
                //reminderSwitch.isOn = false
            }
        }
    }
    
    func scheduleNotification() {
        let calender = Calendar.current
        let content = UNMutableNotificationContent()
        content.title = "Have a great day!"
        content.body = "Would you like to check in and uplift your vibration today?"
        content.sound = UNNotificationSound.default
        
        var dateComponents = DateComponents()
        dateComponents.hour = calender.component(.hour, from: defaultDateTime)
        dateComponents.minute = calender.component(.minute, from: defaultDateTime)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        let request = UNNotificationRequest(identifier:"timeIdentifier", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    
 
    //MARK: TOGGLE OFF NOTIFICATION
    @IBAction func reminderSwitch(_ sender: UISwitch) {
        if sender.isOn == true {
           registerLocalNotification()
           scheduleNotification()
           
        } else {
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        }
        defaults.set(reminderSwitch.isOn, forKey: "SwitchIsOn")
    }
    

    
    // MARK: Direct User to website help page
    @IBAction func helpButtonPressed(_ sender: Any) {
    
        //MARK: Track how many people go to seek help to use the app
        Answers.logContentView(withName: "Help", contentType: "Help from Setting", contentId: "1072")
        if let url = URL(string: "https://upuvibe.com/help/") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    //MARK: Pop up the share function in user's screen
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        // MARK: TRACK HOW MANY USERS USE ADD NOTE BUTTON IN NEW VIBRATION PAGE
        Answers.logShare(withMethod: "system",
                         contentName: "Share from Settings",
                         contentType: "Share the App",
                         contentId: "1050")
        
        //Answers.logCustomEvent(withName: "Share", customAttributes: ["Method":"From Settings"])
        
        let items: [Any] = ["I just use this app UpUVibe to monitor and raise vibration. Try it out and have a nice day. You can download it in here", URL(string: "http://www.upUVibe.com")!]
        let activityVC = UIActivityViewController(activityItems: items, applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    
    
    // MARK: CONTACT US BUTTON
    @IBAction func contactUsButtonPressed(_ sender: Any) {
        sendEmail()
        }
    
  public func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@upuvibe.com"])
            mail.setSubject("contact Email")
            mail.setMessageBody("I have some suggestion!", isHTML: false)
            
            present(mail, animated: true)
        } else {
            print("not sending mail")
        }
        
    }
    
   public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
        case MFMailComposeResult.failed.rawValue:
            print("Error: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    //MARK: TERMS AND CONDITION BUTTON
    @IBAction func termsButtonPressed(_ sender: UIButton) {
        //MARK: Track how many people go to see the terms
        Answers.logContentView(withName: "Terms", contentType: "Terms from Setting", contentId: "1071")
        
        if let url = URL(string: "https://upuvibe.com/terms-conditions/") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
     //MARK: Log out the user and send them back to the WelcomeViewController
    
    @IBAction func signOutButtonPressed(_ sender: UIButton) {
        SVProgressHUD.show()
        do {
            try Auth.auth().signOut()
         

            SVProgressHUD.dismiss()
            // self.dismiss(animated: true, completion: nil)
            print("logout successful")
            navigationController?.popToRootViewController(animated: true)
        } catch let err {
            SVProgressHUD.dismiss()
            errorMessage()
            print(err)
            
        }
    
    }
    
func errorMessage () {
        
        let alert = UIAlertController (title: "No connection, please try again later.", message: ".", preferredStyle: .alert)
        
        
        let action = UIAlertAction(title: "OK", style: .default ) { (Action) in
   
        }
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
        
    }
  
    
}
