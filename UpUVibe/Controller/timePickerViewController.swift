//
//  timePickerViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 1/12/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import Crashlytics

protocol CanReceiveReminder {
    func receiveReminder (data: Date)
}

class timePickerViewController: UIViewController {

    @IBOutlet weak var datePickerOutlet: UIDatePicker!
    
    var delegate : CanReceiveReminder?
    
    var reminderTime = Date()
    
    let dateFormatter = DateFormatter()
    let datePicker = UIDatePicker() // it store both date and time
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Track how many people use set up reminder
        Answers.logContentView(withName: "Reminder", contentType: "Reminder", contentId: "1051")
        
    }
    
    @IBAction func datePickerSelected(_ sender: UIDatePicker) {
        reminderTime = datePickerOutlet.date
    }
    
  
  
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        delegate?.receiveReminder(data: reminderTime)
//        self.dismiss(animated: true, completion: nil) => this is used as module segue
 // This is used to dismiss the push method.
        self.navigationController?.popViewController(animated: false)
    }
    
}


