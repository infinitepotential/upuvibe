//
//  SingleJournalViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 8/12/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import RealmSwift
import Crashlytics

protocol UpdateJournal {
    func updateJournal(_ editedJournal : Journal)
}
class SingleJournalViewController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var journalDateLabel: UILabel!
    @IBOutlet weak var titleTextField: BorderUITExtField!
    @IBOutlet weak var contentTextView: UITextView!
    
    let realm = try! Realm()
    var journals : Results<Journal>?
    var editedJournal = Journal()
    let formatter = DateFormatter()
    var delegate : UpdateJournal?
  
    
    var selectedJournal : Journal? { //Journal?
        didSet {
            loadJournal ()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //MARK: Track how many people edit journal
        Answers.logContentView(withName: "Edit Journal", contentType: "Edit Journal", contentId: "1042")
        
    contentTextView.delegate = self
    contentTextView.tintColorDidChange() // This needs to be called to make the cursor black, besides change the tint color in storyboard.
        titleTextField.tintColorDidChange()
           loadJournal ()
    }
    
    //MARK: DATASOURCE 
    func loadJournal() {
       
        formatter.dateFormat = "E, d MMM yyyy h:mm a"
        if let journalDate = selectedJournal?.dateCreated {
            journalDateLabel?.text = formatter.string(from: journalDate)
            titleTextField?.text = selectedJournal?.title
            contentTextView?.text = selectedJournal!.content
      }
        else {
            print("Problem of uploading journals")
        }
        
        if contentTextView?.text == "" {
            contentTextView.textColor = UIColor.lightGray
            contentTextView?.text = "Please write here..."
            contentTextView.tintColorDidChange()
        }
    }
    
   
    //MARK: UPDATE REALM DATA
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        if contentTextView.text! == "Please write here..."  { // && contentTextView.textColor == UIColor.lightGray
            editedJournal.content = ""
        } else {
             editedJournal.content = contentTextView.text!
        }
        editedJournal.title = titleTextField.text!
        editedJournal.dateCreated = selectedJournal?.dateCreated
        delegate?.updateJournal(editedJournal)
        self.navigationController?.popViewController(animated: true)

    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Please write here..." {
            textView.text = ""
            textView.textColor = UIColor.black
        //   textView.font = UIFont(name: "San Franciso", size: 17.0)
        }
    }

    
    

    
}



