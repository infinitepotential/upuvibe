//
//  HomeViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 23/11/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import Crashlytics
import MessageUI 


class HomeViewController: UIViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var label: UIPaddedLabel!
    @IBOutlet weak var settingButton: UIBarButtonItem!
    
    @IBOutlet weak var tipsButton: UIBarButtonItem!
    @IBOutlet weak var vibeButton: UIBarButtonItem!
    
    @IBOutlet weak var journalButton: UIBarButtonItem!
    
    let currentCount = UserDefaults.standard.integer(forKey: "launchCount")
    //    @IBOutlet weak var settingButtonPressed: UIBarButtonItem!
   
    override func viewWillAppear(_ animated: Bool) {
        
      //  self.navigationItem.leftBarButtonItem = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //MARK: Check User time for Review
       // checkAndAskForRating(currentCount)
       // checkAndAskForReview(currentCount)
        
        //MARK: Track how many people use home page
        Answers.logContentView(withName: "Home", contentType: "Home", contentId: "1020")
        
        //this set the Home page for the next level screen
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Home", style: .plain, target: nil, action: nil)
        self.navigationItem.setHidesBackButton(true, animated:true)
        self.navigationItem.hidesBackButton = true
        navigationController?.navigationBar.topItem?.hidesBackButton = true
        //self.navigationItem.backBarButtonItem?.title = ""
       // self.navigationItem.backBarButtonItem?.tintColor = UIColor.clear
        
//        // Show navigation controller’s built-in toolbar
//        navigationController?.setToolbarHidden(false, animated: false)

  
        //MARK:  Formatting the Vibrational scale article page ( TextView)
        label.layer.cornerRadius = 10.0
        label.layer.shadowColor = UIColor.gray.cgColor
        label.layer.shadowOffset = .zero
        label.layer.shadowOpacity = 0.6
        label.layer.shadowRadius = 10
        label.layer.shadowPath = UIBezierPath(rect: label.bounds).cgPath
        label.layer.shouldRasterize = true
        

    
  //  @IBAction func crashButtonPressed(_ sender: UIButton) {
 //        Crashlytics.sharedInstance().crash()
//    }
        
        //MARK: Formattting the title, headings and highlighted text etc. 
       //Set the label.text! to MutableAttributedString object
        let stringValue = label.text!
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
        
        attributedString.setBigText(textForAttribute: "About Vibrational Scale")
        attributedString.setBoldText(textForAttribute: "A Brief Explanation")
        attributedString.setBoldText(textForAttribute: "What is the World’s Vibration?")
        attributedString.setBoldText(textForAttribute: "Why Higher Vibration?")
        attributedString.setBoldText(textForAttribute: "What Can Affect Your Vibration?")
        attributedString.setBoldText(textForAttribute: "How Can You Raise Your Vibration?")
        attributedString.setBoldText(textForAttribute: "Use Our App to Achieve Your Goal")
        attributedString.setColorText(textForAttribute: "become aware")
        attributedString.setColorText(textForAttribute: "focus on what can raise your consciousness")
        attributedString.setColorText(textForAttribute: "Building positive habits")
       
        label.attributedText = attributedString
 
        
    }

   func checkAndAskForReview(_ currentCount : Int) {
        // this will not be shown everytime. Apple has some internal logic on how to show this.
        
        switch currentCount {
        case 7, 35:
            requestReview()
        default:
            print("App run count is : \(currentCount)")
            break;
        }
    
    }

    func requestReview() {
        
        //Set up alert to ask whether user enjoy using our app, if yes, ask them whether they would love to leave us a review? if No, direct them either to help page or write to us to improve.
        let alert = UIAlertController (title: "Do you enjoy using our app?", message: "", preferredStyle: .alert)
        
        //If user answer yes : ask them for review (1) yes, call rateApp func (2) cancel
        let rateAppAction = UIAlertAction(title: "Yes", style: .default ) { (Action) in
            
            
            let pleaseRateAppAlert = UIAlertController(title: "Would you like to help us by reviewing us in App Store?", message: "", preferredStyle: .alert)
            
            pleaseRateAppAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                
                
            pleaseRateAppAlert.addAction(UIAlertAction(title: "Yes", style: .default) { (action) in
                self.rateApp(appId: "id1451469413")
            })
            
            self.present(pleaseRateAppAlert, animated: true, completion: nil)
        }
        
        let userHaveIssueAction = UIAlertAction(title: "No", style: .default ) { (Action) in
            
            
            let userHaveIssueAlert = UIAlertController(title: "It seems that you are having issues.", message: "Would you tell us what they are or need some clarifications?", preferredStyle: .alert)
            
            userHaveIssueAlert.addAction(UIAlertAction(title: "Report", style: .default) {(action) in
                self.sendEmail()
                
                }
            )
            
            userHaveIssueAlert.addAction(UIAlertAction(title: "Confused", style: .default) { (action) in
                if let url = URL(string: "https://upuvibe.com/help/") {
                    UIApplication.shared.open(url, options: [:])
                }
            })
            
          self.present(userHaveIssueAlert, animated: true, completion: nil)
        }
        
        
        alert.addAction(userHaveIssueAction)
        alert.addAction(rateAppAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    
  func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@upuvibe.com"])
            mail.setSubject("contact Email")
            mail.setMessageBody("Report Issue:", isHTML: false)
            
            present(mail, animated: true)
        } else {
            print("not sending mail")
        }
        
    }
    
func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
        case MFMailComposeResult.failed.rawValue:
            print("Error: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    //This one is working
   
    fileprivate func rateApp(appId: String) {
        openUrl("itms-apps://itunes.apple.com/app/" + appId)
    }
    
    fileprivate func openUrl(_ urlString:String) {
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

}
    
//    //Option 1: Rating option
//        func rateApp(id : String) {
////            guard let url = URL(string : "itms-apps://itunes.apple.com/app/id\(id)?mt=8&action=write-review") else { return }
////            if #available(iOS 10.0, *) {
////                UIApplication.shared.open(url, options: [:], completionHandler: nil)
////            } else {
////                UIApplication.shared.openURL(url)
////            }
//        }
//
    
    





//Option 2: Rating option
//    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
//        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
//            completion(false)
//            return
//        }
//        guard #available(iOS 10, *) else {
//            completion(UIApplication.shared.openURL(url))
//            return
//        }
//        UIApplication.shared.open(url, options: [:], completionHandler: completion)
//    }
    
    
    


