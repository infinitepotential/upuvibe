//
//  AppDelegate.swift
//  UpUVibe
//
//  Created by Catherine Chan on 21/11/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import RealmSwift
import SwipeCellKit
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print(Realm.Configuration.defaultConfiguration.fileURL ?? "Cannot print file path")
        
        //MARK: get current number of times app has been launched
     let currentCount = UserDefaults.standard.integer(forKey: "launchCount")
        
        //MARK: increment received number by one
        UserDefaults.standard.set(currentCount+1, forKey:"launchCount")
        
        
        //TODO:  Change the status bar color to white
            UINavigationBar.appearance().barStyle = .blackOpaque
        
        //TODO: initializing crashlytics.
            //  Fabric.with([Crashlytics.self]) => this is only initializing crashlytics without Answers
        
            Fabric.with([Answers.self]) // inittailizing crashlytics with Answers
        
        // TODO:  Initialise and configure Firebase
        FirebaseApp.configure() // This is called before anything is lauching in our app
        
        let myDatabase = Database.database().reference()
        myDatabase.setValue("We have got data")
        
        //TODO: IQKeyboardManager
        IQKeyboardManager.shared.enable = true
        
        //TODO: Init Realm Object
        do {
            _ = try Realm()
        } catch {
            print("Error in initializing Realm: \(error)")
        }
        
        return true
    }
}

