//
//  MyTipsTableViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 24/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import RealmSwift
import Crashlytics

class MyTipsTableViewController: SwipeTableTableViewController {
// UITextFieldDelegate is important to make the things work for the cell. It should has to do with the re-order function , UITextFieldDelegate
        
        let realm = try! Realm()

        
        var tips  : List<Tip>? // List of tip Objects
        var tipWrapper = TipsWrapper() // create an empty list object
        var tip = Tip()
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            //MARK: Track how many people use myTips page
            Answers.logContentView(withName: "My Tip", contentType: "My Tip", contentId: "1033")
            
            // Register "newTableViewCell" with nibFile: "NewTableViewCell", which is the same name of xib file, just look the xib name.
            tableView.register(UINib(nibName: "AffirmationCell", bundle: nil),  forCellReuseIdentifier: "affirmationCell")
            
            
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 100
            tableView.tableFooterView = UIView() // remove extra cell
            
            self.tableView.isEditing = true
            loadTip()
            tableView.setEditing(false, animated: true)
        }
        
        // MARK: - Table view data source
        
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return tips?.count ?? 1
            
        }
        
        
        
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "affirmationCell", for: indexPath) as! AffirmationCell // the 1st is the Identifier : indentity of the cell, the 2nd is the class of the cell
            
            cell.delegate = self
            
            if let currentTip = tips?[indexPath.row]
               
                
            {
                cell.label.textColor = UIColor.black
                cell.label.text =  currentTip.content
                cell.label.lineBreakMode = .byWordWrapping
                cell.label.numberOfLines = 0
                
                
            } else {
                cell.label.textColor = UIColor.lightGray
                cell.label.text = "Please add your first tip."
            }
            
            
            return cell
        }
        
        
        override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
            
        }
        
        
        //MARK: Reorder function
        
        //Show the reoder symbol
        override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
            return .none
        }
        
        //Determine whehter the tableView shall indent while editing
        override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
            return false
        }
        
        
        // Determine whether a particular can be move
        override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
            return true
        }
        
        
        override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
            try! realm.write {
                
                let movedObject = tips![sourceIndexPath.row]
                tips!.remove(at: sourceIndexPath.row)
                tips!.insert(movedObject, at: destinationIndexPath.row)
                
            }
            
            
            
            tableView.reloadData()
        }
    
    
    //MARK: Update Realm DataBase When delete
        override func updateModel (at indexPath: IndexPath) {
            
            
            if let affirmationforDeletetion = self.tips?[indexPath.row] {
                
                do {
                    try self.realm.write {
                        self.realm.delete(affirmationforDeletetion)
                        
                    }
                    
                    self.loadTip()
                } catch {
                    print("Error deleting Affirmation:\(error)")
                }
                //  tableView.reloadData()
                // self.loadAffirmation()
                
            }
            
        }
        
        //MARK: Load affirmation results
        func loadTip() {
            
            if let results = (realm.objects(TipsWrapper.self).first?.list) {
                tips = results
                tipWrapper.list = tips!
                //    tipWrapper.list = results
            } else {
                
            }
            
            
            tableView.reloadData()
            
        }
        
        @IBAction func reOrderButtonPressed(_ sender: UIButton) {//if the tableView is in editing mode, it will set a editing mode for us to reorder
            if (tableView.isEditing == true) {
                tableView.setEditing(false, animated: true)
            } else {
                tableView.setEditing(true, animated: true)
            }
            
            
        }
        
        
        
    //MARK: ADD NEW TIPS
        @IBAction func newTipButtonPressed(_ sender: UIButton) {
            var textField = UITextField()
            
            
            let alert = UIAlertController (title: "Add New Tip", message: "", preferredStyle: .alert)
            
            
            let action = UIAlertAction(title: "Add", style: .default ) { (Action) in
                
                let newTip = Tip()
                
                newTip.content = textField.text!
                
                self.saveTip(newTip)
                
            }
            
            alert.addTextField{ (alertTextField) in
                
                alertTextField.placeholder = "Add Your Tip Here..."
                
                textField = alertTextField
                
            }
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(action)
            
            present(alert, animated: true, completion: nil)
        }
        
        
        
        func saveTip(_ tip: Tip) {
            
            do {
                try realm.write {
                    tipWrapper.list.append(tip)
                    if tips == nil {
                        realm.add(tipWrapper)
                    }
                }
            } catch {
                print("Error initalising and saving tip:\(error)")
            }
            
            self.loadTip()
        }
        
}


