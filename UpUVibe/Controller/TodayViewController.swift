//
//  TodayViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 28/12/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import Charts
import RealmSwift
import Crashlytics

class TodayViewController: UIViewController, LoadData  {
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yesterdayButton: UIButton!
    @IBOutlet weak var todayButton: UIButton!
    
    let timeScale = ["12am","3am","6am","9am","12pm","3pm","6pm","9pm","12am"]
    let timeValue : [Double] = [0]
    var vibrationScore: [Double] = []
    let realm = try! Realm()
    var vibration : [Vibration] = []
    var dataEntry: [ChartDataEntry] = []
    var todayStart = Date() // return the
//    var todayEnd = Date()
    var date = Date()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK: Track how many people use daily score
        Answers.logContentView(withName: "Daily Vibration", contentType: "Daily Vibration", contentId: "1060")

//        self.navigationController?.navigationBar.topItem?.title = "Home"
        yesterdayButton.applyButtonDesign(hex: 0x420093)
        todayButton.applyButtonDesign(hex: 0x420093)
        
     loadData()
     
    }
    
    //MARK: LOAD UP DATA FOR LINE CHART
    func loadData(){
        todayStart = Calendar.current.startOfDay(for: date)
        let todayEnd = { () -> Date in
            let components = DateComponents(day: 1, second: -1)
            return Calendar.current.date(byAdding: components, to: todayStart)!
        }()
        dataEntry = []
        vibration = Array(realm.objects(Vibration.self).filter("dateCreated BETWEEN %@", [todayStart, todayEnd]).sorted(byKeyPath: "dateCreated", ascending: true))
        
        lineChartSetUp()
        
    }
    
    //MARK: SET UP LINE CHART GRAPH
    func lineChartSetUp(){
        
        //Line Chart Animation
        lineChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInSine)
        
        //set up datapoint
        for i in 0..<vibration.count {
            let elapsedTime = vibration[i].dateCreated!.timeIntervalSince(todayStart)
            let time = elapsedTime / 60 / 60 / 3
            let dataPoint = ChartDataEntry(x: time, y: vibration[i].score)
            dataEntry.append(dataPoint)
        }
        
        let chartDataSet = LineChartDataSet(entries: dataEntry, label: "Today Score")
        let chartData = LineChartData()
        chartData.addDataSet(chartDataSet)
        chartData.setDrawValues(false) // set value of the graph
        chartDataSet.colors = [UIColor.purple]
        
        //for cubit chart
        chartDataSet.mode = .cubicBezier
        chartDataSet.cubicIntensity = 0.2
        chartDataSet.drawCirclesEnabled = true // datapoint circle
        chartDataSet.circleRadius = 5
        chartDataSet.circleColors =  [UIColor.purple]
        chartDataSet.valueFont = UIFont(name: "Helvetica", size: 12.0)! // font of the value of the graph
        
        //Axes Set up
        
        let formatter : ChartFormatter = ChartFormatter(labels: timeScale)
        
        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.xAxis.valueFormatter = formatter
        lineChartView.xAxis.axisMinimum = 0
        lineChartView.xAxis.axisMaximum = 8.5
       
        
        
        lineChartView.chartDescription?.enabled = false
        lineChartView.legend.enabled = false
        lineChartView.rightAxis.enabled = false
        lineChartView.leftAxis.drawGridLinesEnabled = false
        lineChartView.data = chartData
        lineChartView.leftAxis.axisMaximum = 750
        lineChartView.leftAxis.axisMinimum = 0
        lineChartView.leftAxis.minWidth = 30
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToVibration" {
            let destinationVC = segue.destination as! VibrationViewController
            destinationVC.delegate = self
        }
    
}
    
    
 
    @IBAction func yesterdayButtonPressed(_ sender: UIButton) {
        titleLabel.text =  "Yesterday Score"
        date = Calendar.current.date(byAdding: .day, value: -1, to: Date())!
        loadData()
    }
    
    
    @IBAction func todayButtonPressed(_ sender: Any) {
       titleLabel.text = "Today Score"
     date = Date()
        loadData()
    }
    
}



