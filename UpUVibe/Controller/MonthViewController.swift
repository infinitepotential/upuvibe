//
//  MonthViewController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 28/12/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import Charts
import RealmSwift
import Crashlytics

class MonthViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var lastMonthButton: UIButton!
    @IBOutlet weak var thisMonthButton: UIButton!
    
    var dataEntry : [ChartDataEntry] = []
    let timeScale = ["  Day", "1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31",]
    var timeValue : [Double] = []
    var vibrationScore : [Double] = []
    let realm = try! Realm()
    var vibration : [Vibration] = []
    let calendar = Calendar.current
    var date = Date()
    let timeInterval = 24.0 * 3600.0
    var dayArray : [Date] = []
    var day = 28
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        //MARK: Track how many people use monthly score
        Answers.logContentView(withName: "Monthly Vibration", contentType: "Monthly Vibration", contentId: "1062")
        
        lastMonthButton.applyButtonDesign(hex: 0x420093)
        thisMonthButton.applyButtonDesign(hex: 0x420093)
        loadData()
        
    }
    
    //MARK: LOAD DATA FROM REALM
    func loadData(){
        //Empty the data to repopulate
        dataEntry = []
        dayArray = []
        vibrationScore = []
        timeValue = []
        let monthInterval =
            calendar.dateInterval(of: .month, for: date)! // calculate duration of the current month
        day = calendar.dateComponents([.day], from: monthInterval.start, to: monthInterval.end).day! // calculate the days of the current month
        dayArray.append(monthInterval.start)
        
        // MARK: POPULATE DATAPOINT FOR EACH AVAILABLE DAILY SCORE
                for i in 0...day-1 {
                    var score : Double = 0
                    let nextDay = dayArray[i].addingTimeInterval(timeInterval)
                    dayArray.append(nextDay)
        
                    vibration = Array(realm.objects(Vibration.self).filter("dateCreated BETWEEN %@", [dayArray[i], dayArray[i+1]]))
                    
                    if vibration.count != 0 { // if there is data of the day
                        timeValue.append(Double(i)) // append the day value (day 1 .. day 31)
                     
                        
                        for j in 0..<vibration.count { // calculate the total score for the day
                            score = score + vibration[j].score
                        }
                    vibrationScore.append(score/Double(vibration.count)) // append daily average score to array When it is not nil
                }
                    
        }
       
        lineChartSetUp()
        
    }
    
    //MARK: SET UP LINE CHART GRAPH
    func lineChartSetUp(){

        //Line Chart Animation
        lineChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInSine)

        for i in 0..<timeValue.count {
            let dataPoint = ChartDataEntry(x: timeValue[i]+1, y: vibrationScore[i])
            dataEntry.append(dataPoint)
        }

        let chartDataSet = LineChartDataSet(entries: dataEntry, label: "Average Daily Score")
        let chartData = LineChartData()
        chartData.addDataSet(chartDataSet)
        chartData.setDrawValues(false) // set value of the graph
        chartDataSet.colors = [UIColor.purple]
        
        //for cubit chart
        chartDataSet.mode = .cubicBezier
        chartDataSet.cubicIntensity = 0.2
        chartDataSet.drawCirclesEnabled = true // datapoint circle
        chartDataSet.circleRadius = 5
        chartDataSet.circleColors =  [UIColor.purple]
        
        //chartDataSet.valueFont = UIFont(name: "San Francisco", size: 12.0)! // font of the value of the graph

        //Axes Set up

        let formatter : ChartFormatter = ChartFormatter(labels: timeScale)

        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.xAxis.drawGridLinesEnabled = false
        lineChartView.xAxis.valueFormatter = formatter
        lineChartView.xAxis.axisMinimum = 0
        lineChartView.xAxis.axisMaximum = Double(day)
    

        lineChartView.chartDescription?.enabled = false
        lineChartView.legend.enabled = false
        lineChartView.rightAxis.enabled = false
        lineChartView.leftAxis.drawGridLinesEnabled = false
        lineChartView.data = chartData
        lineChartView.leftAxis.axisMaximum = 750
        lineChartView.leftAxis.axisMinimum = 0
        lineChartView.leftAxis.minWidth = 30


   }
    
    @IBAction func lastMonthButtonPressed(_ sender: UIButton) {
        titleLabel.text = "Last Month Score"
        date = calendar.date(byAdding: .month, value: -1, to: date)!
        loadData()
        
    }
    
    @IBAction func thisMonthButtonPressed(_ sender: UIButton) {
        titleLabel.text = "This Month Score"
        date = Date()
        loadData()
    }
    
}
   
