//
//  AffirmationCell.swift
//  UpUVibe
//
//  Created by Catherine Chan on 21/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import SwipeCellKit

class AffirmationCell: SwipeTableViewCell  {

   // @IBOutlet weak var affirmationLabel: UILabel!
    

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
