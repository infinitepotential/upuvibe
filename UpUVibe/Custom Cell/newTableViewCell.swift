//
//  newTableViewCell.swift
//  UpUVibe
//
//  Created by Catherine Chan on 14/12/18.
//  Copyright © 2018 Infinite Potential App Ltd. All rights reserved.
//

import UIKit

class NewTableViewCell: UITableViewCell {

    @IBOutlet weak var journalDayUILabel: UILabel!
    @IBOutlet weak var journalYearUILabel: UILabel!
    @IBOutlet weak var journalTitleUILabel: UILabel!
    @IBOutlet weak var journalTimeUILabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
