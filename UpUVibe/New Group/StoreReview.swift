//
//  StoreReviewHelper.swift
//  UpUVibe
//
//  Created by Catherine Chan on 31/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import Foundation
import StoreKit


//struct StoreReviewHelper {
    
public func checkAndAskForRating(_ currentCount : Int) {
        // this will not be shown everytime. Apple has some internal logic on how to show this.

        switch currentCount {
        case 20,50:
            requestRating()
        case _ where currentCount%100 == 0 :
            requestRating()
        default:
            print("App run count is : \(currentCount)")
            break;
        }

    }

    fileprivate func requestRating() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            // Fallback on earlier versions
            // Try any other 3rd party or manual method here.
        }
    }
//}



