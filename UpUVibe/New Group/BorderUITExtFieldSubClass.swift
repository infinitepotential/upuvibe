//
//  BorderUITExtFieldSubClass.swift
//  UpUVibe
//
//  Created by Catherine Chan on 28/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import UIKit

//MARK: UI Text Field Formating
class BorderUITExtField : UITextField {
    override func didMoveToWindow() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor(rgb: 0x420093).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
