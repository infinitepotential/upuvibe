//
//  UIPaddedLabel.swift
//  UpUVibe
//
//  Created by Catherine Chan on 28/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import UIKit

public class UIPaddedLabel: UILabel {
    // Declare the inset value of float
    @IBInspectable var topInset: CGFloat = 25.0
    @IBInspectable var bottomInset: CGFloat = 25.0
    @IBInspectable var leftInset: CGFloat = 17.0
    @IBInspectable var rightInset: CGFloat = 15.0
    
    public override func drawText(in rect: CGRect) {
        
        //Creates an edge inset for a button or view.
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        
        //Draws the label's text (or its shadow) in the specified rectangle. Returns a rectangle that is smaller or larger than the source rectangle, with the same center point.
        super.drawText(in: rect.inset(by: insets))
    }
    
    //change the instrinsicContentSize of the UIPaddedLabel by adding the left and right padding, height by adding the top and bottom margin. Because of adding the padding, the size of the contents actually increase.
    public override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
    
    //resize the current view so that it uses the most appropriate amount of space.
    public override func sizeToFit() {
        super.sizeThatFits(intrinsicContentSize)
    }
    
    
    
    
}


