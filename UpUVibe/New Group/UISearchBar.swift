//
//  UISearchBar.swift
//  UpUVibe
//
//  Created by Catherine Chan on 28/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import UIKit
import RealmSwift

//MARK: Search Bar Delegate Method
extension JournalSummaryTableViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        journals = Array(realm.objects(Journal.self).filter("title CONTAINS[cd] %@", searchBar.text!).sorted(byKeyPath: "dateCreated", ascending: false))
        
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            loadJournal()
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
            
        }
    }
    
}

