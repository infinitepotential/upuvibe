//
//  UIAlertController.swift
//  UpUVibe
//
//  Created by Catherine Chan on 28/01/19.
//  Copyright © 2019 Infinite Potential App Ltd. All rights reserved.
//

import UIKit

extension UIAlertController{
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.tintColor = UIColor(rgb: 0x420093)
    }
}

